const originLat = 44.532308;
const originLong = -87.917582;

targetLoc = { 'latitude': originLat, 'longitude': originLong };
distance = 100;

$(document).ready(function () {
    $('#currentUsername i').html('Anonymous' + Math.floor(Math.random() * 20000));
});

function updateUsername() {
    $('#currentUsername i').html($('#username').val());
    $('#username').val('').blur();
}

function setLocation(newLat, newLong) {
    $.ajax({
        method: 'POST',
        url: '/',
        data: JSON.stringify({ "distance": 1, "username": 'Nobody!', newLocation: { "newLat": newLat, "newLong": newLong } }),
        success: handleResponse
    });
}

function randomLocation() {
    $.ajax({
        method: 'POST',
        url: '/',
        data: JSON.stringify({ "distance": 1, "username": 'Nobody!' }),
        success: handleResponse
    });
}

// From http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula/27943
function getDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    distance = d * 1000;
    return d * 1000;
}

// From http://stackoverflow.com/questions/27928/calculate-distance-between-two-latitude-longitude-points-haversine-formula/27943
function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

function displayLocation(position) {
    $('div.distance p').html('Distance: ' + getDistance(position.coords.latitude, position.coords.longitude, targetLoc.latitude, targetLoc.longitude).toFixed(2) + ' meters' + '<br>Accr: ' + position.coords.accuracy);
}

var geo_options = {
    enableHighAccuracy: true,
    maximumAge: 0,
    timeout: 1000
};

if (navigator.geolocation) {
    navigator.geolocation.watchPosition(displayLocation, function () { }, geo_options);
}

function vibrate(duration) {
    navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
    if ('vibrate' in navigator) {
        navigator.vibrate(duration);
    }
}

function getCurrentTarget() {
    username = $('#currentUsername i').html();
    $.ajax({
        method: 'POST',
        url: '/',
        data: JSON.stringify({ "distance": distance, "username": username }),
        success: handleResponse
    });
}

function handleResponse(data) {
    data = JSON.parse(data);
    if (data.latitude != targetLoc.latitude || data.longitude != targetLoc.longitude) {
        vibrate(500);
        var marker = {
            lat: data.latitude,
            lng: data.longitude
        };
        map.removeMarkers();
        map.addMarker(marker);

    }
    $('div.lastWinner p').html('Last Winner: <strong>' + data.lastWinner + '</strong>');
    targetLoc = data;
    navigator.geolocation.getCurrentPosition(displayLocation);
}

var map = null;
var markers = [];

function initMap() {
    map = new GMaps({
        div: '#map',
        lat: originLat,
        lng: originLong,
        zoom: 16
    });
    $('#map').css('width', '100%');
    getCurrentTarget();
    setInterval(getCurrentTarget, 2500);
}