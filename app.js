//Lets require/import the HTTP module
var http = require('http');
var fs = require('fs');
var url = require('url');

//Lets define a port we want to listen to
const PORT = 80;

var responseData = { "latitude": 44.5337, "longitude": -87.9174 };
var lastWinner = 'Nobody!';

//We need a function which handles requests and send response
function handleRequest(request, response) {
    if (request.method == 'GET') {
        response.writeHead(200, { 'Content-Type': 'text/html', 'Access-Control-Allow-Origin': '*' });
        if (request.url == '/') {
            path = 'index.html';
            stream = fs.createReadStream(path);
            stream.on('error', function () {
                respond404(response);
            });
            stream.pipe(response);
        } else {
            path = url.parse(request.url).pathname.substring(1);
            stream = fs.createReadStream(path);
            stream.on('error', function () {
                respond404(response);
            });
            stream.pipe(response);
        }
    } else if (request.method == 'POST') {
        response.writeHead(200, { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' });
        var body = [];
        request.on('data', function (chunk) {
            body.push(chunk);
        }).on('end', function () {
            body = Buffer.concat(body).toString();
            body = JSON.parse(body);
            console.log(JSON.stringify(body));
            if(body.newLocation != null) {
                responseData = setPosition(body.newLocation.newLat, body.newLocation.newLong);
                lastWinner = body.username;
            } else if (body.distance < 4) {
                responseData = nextPosition();
                lastWinner = body.username;
            }
            responseData.lastWinner = lastWinner;
            response.write(JSON.stringify(responseData));
            response.end();
        });
    } else {
        respond404(response);
    }
}

function respond404(response) {
    response.writeHead(404, { 'Content-Type': 'text/plain', 'Access-Control-Allow-Origin': '*' });
    response.write('Error 404: Not found!');
    response.end();
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function () {
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
});

function nextPosition() {
    lat1 = 44.532802 * (Math.PI / 180);
    long1 = -87.917733 * (Math.PI / 180);
    radiusEarth = 3960.056052; // Miles
    maxdist = 0.05; // Miles
    maxdist = maxdist / radiusEarth;

    rand1 = Math.random();
    rand2 = Math.random();
    dist = Math.acos(rand1 * (Math.cos(maxdist) - 1) + 1);
    brg = 2 * Math.PI * rand2;
    lat2 = Math.asin(Math.sin(lat1) * Math.cos(dist) + Math.cos(lat1) * Math.sin(dist) * Math.cos(brg));
    long2 = long1 + Math.atan2(Math.sin(brg) * Math.sin(dist) * Math.cos(lat1), Math.cos(dist) - Math.sin(lat1) * Math.sin(lat2));
    lat2 = lat2 * 180 / Math.PI;
    long2 = long2 * 180 / Math.PI;
    // console.log(lat2 + ', ' + long2);
    pos = {}
    pos.latitude = lat2;
    pos.longitude = long2;
    return pos;
}

function setPosition(newLat, newLong) {
    // console.log(newLat, newLong);
    pos = {}
    pos.latitude = parseFloat(newLat);
    pos.longitude = parseFloat(newLong);
    return pos;
}

// setInterval(function() { responseData = nextPosition(); }, 100);